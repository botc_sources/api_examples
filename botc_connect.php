<?php

  define('APICLIENT', Array(
      'grant_type' => getenv('API_GRANT_TYPE'),
      'username'  => getenv('API_USERNAME'),
      'password'  => getenv('API_PASSWORD'),
      'client_id' => getenv('API_CLIENT_ID'),
      'client_secret' => getenv('API_CLIENT_SECRET')
    )
  );

  define('BOTC_API', getenv('BOTC_API') );

  define('TX_EXPIRED',900);
  define('TX_CONFIRMATIONS',1);

  define('TX_URL_NOTIFICATION','https://example.com/mynotificationscript.php');

  define('TIMEOUT_PAY_IN',900);
  define('TIMEOUT_PAY_IN_RETRY',1);

  define('TIMEOUT_PAY_OUT',43200);
  define('TIMEOUT_PAY_OUT_RETRY',6);

?>
