<?php

/**
 *
 */
class BotC
{
  private $auth_token;
  public $balances;


  function __construct() {
    $this->auth();
  }

  function auth(){

    $J = json_decode( $this->request(BOTC_API.'/oauth/v2/token', true, APICLIENT),true );
    if( isset( $J['access_token'] )) {
      $this->auth_token = $J['access_token'];
    } else {
      $this->auth_token = false;
    }
  }

  function isConnected(){
    return ( $this->auth_token == false ) ? false : true;
  }

  function request($url, $post, $param){
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url );
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    'Authorization: Bearer '.$this->auth_token
    ));
    if( $post == true ){
      curl_setopt($ch, CURLOPT_POST,true);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $param );
    }

    $data = curl_exec($ch);
    curl_close($ch);
    return $data;

  }

  function getBalances(){
    $J = json_decode( $this->request(BOTC_API.'/user/v1/wallet', false, ""),true );
    if( $J['status'] == 'error' ){
      $this->balances=0;
      return;
    }
    foreach( $J['data'] as $j ){
      if( $j['status'] == 'enabled' ) $this->balances[ $j['currency'] ] = $j['available'] / pow( 10, $j['scale']);
    }
  }

  function getTxs( $limit=15, $offset=0, $query='' ){
    return json_decode( $this->request(BOTC_API.'/user/v1/wallet/transactions?limit='.$limit.'offset='.$offset.'&'.$query , false, '' ) ,true );
  }

  function getTx($botc_tx_id,$tx_type){
    return json_decode( $this->request(BOTC_API.'/methods/v1/'.$tx_type.'/'.$botc_tx_id, false, ''),true );
  }

  function receiveFAIR( $PaymentRequest ){
    return json_decode( $this->request(BOTC_API.'/methods/v1/in/fac', true, $PaymentRequest ),true );
  }

  function sendFAIR( $SendFAIR ){
    return json_decode( $this->request(BOTC_API.'/methods/v1/out/fac', true, $SendFAIR ),true );
  }
  
  function send_SEPA( $SendSEPA ){
    return json_decode( $this->request(BOTC_API.'/methods/v1/out/sepa', true, $SendSEPA ),true );
  }
 

}

class PaymentRequest
{
  public $currency;
  public $amount;
  public $confirmations;
  public $expires_in;
  public $concept;
  public $url_notification;

  function __construct($currency='fac') {
    $this->currency=$currency;
    $this->confirmations=TX_CONFIRMATIONS;
    $this->expires_in=TX_EXPIRED;
    $this->url_notification='';
  }

  function create($amount,$concept){
    if($iphash) $this->url_notification=TX_URL_NOTIFICATION;
    $this->amount=number_format( $amount * 100000000,0,'','')*1;
    $this->concept=$concept;
  }
}

class SendFaircoin
{
  public $amount;
  public $currency;
  public $address;
  public $concept;
  public $url_notification;
  public $pin;

  function __construct() {
    $this->currency='FAC';
    $this->url_notification='';
  }

  function create($address,$amount,$concept,$pin){
    if($iphash) $this->url_notification=TX_URL_NOTIFICATION;
    $this->amount=number_format( $amount*100000000,0,'','');
    $this->address=$address;
    $this->concept=$concept;
    $this->pin=$pin;
  }
}

/*
SEPA will sent by support members -> tx added to DB only
*/

class SendSEPA
{
  public $currency;
  public $amount;
  public $beneficiary;
  public $iban;
  public $bic_swift;
  public $concept;
  public $url_notification;

  function __construct() {
    $this->currency='EUR';
    $this->url_notification='';
  }

  function create($amount,$beneficiary,$iban, $bic_swift,$concept){
    if($payment_id) $this->url_notification=TX_URL_NOTIFICATION;
    $this->amount=number_format( $amount*100,0,'','');
    $this->beneficiary=utf8_encode( $beneficiary );
    $this->iban=$iban;
    $this->bic_swift=$bic_swift;
    $this->concept=$concept;
  }
}




?>
